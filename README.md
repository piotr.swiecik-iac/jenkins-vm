# jenkins-vm



## Overview

Jenkins deployment on single VM with Ansible & CasC. 

Vagrant machine is used for local testing & fine-tuning before deploying to remote server. For actual deployment, any
Debian-compatible remote machine can be used. Example Terraform config is provided to run project on AWS cloud.



## Requirements

- [ ] Oracle VirtualBox - tested on 7.0.10
- [ ] 2 GB free RAM
- [ ] 2 vCPU capacity


## Running locally with Vagrant

This setup is used for local testing - playing around with settings and pipelines before deploying them to remote server.

All components (Jenkins controller, Jenkins agent, container registry) run on shared Vagrant machine as separate Docker
containers. Data is persisted until VM is deleted with `vagrant destroy.`

Before starting Vagrant, make sure to prepare required key pairs. Use the provided `keygen.sh` script to produce keys automatically
or generate your own.
- [ ] `vagrantkey` is stored in project base dir and used to interact with Vagrant VM via SSH,
- [ ] `gitkey` is stored in stored in `./ansible/` and used to connect to your git repository in CICD jobs. This step
is optional. You can ignore the auto-injected key and add it manually using Jenkins GUI after server is initialized. Of course,
remember to add public key part to your git account on GitHub/Gitlab/Bitbucket.

Start VM with `vagrant up --provision`. Vagrant will execute the following steps:
- [ ] Pull generic Ubuntu 22.04 image from Vagrant Cloud.
- [ ] Launch single VM using Virtualbox.
- [ ] Attach bridge network interface with IP 192.168.56.10 to the machine.
- [ ] Forward port 8080 from VM to localhost.
- [ ] Inject `vagrantkey` public key part into VM.
- [ ] Copy all required files to server.
- [ ] Launch Ansible provisioner and execute `vagrant_init.yaml` playbook:
  - Install required apt pacakges.
  - Install required Python libs.
  - Parse `gitkey` private key part.
  - Use Jinja template to insert git key into Jenkins CasC file.
  - Inject CasC file into VM.
  - Run Jenkins with custom plugin list & execute CasC.
  - Start Jenkins SSH worker node in container.
  - Connect Jenkins node to controller.
 
After provisioning is finished, Jenkins will be running on `http://localhost:8080` with default `admin/admin` credentials.

