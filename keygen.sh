#!/bin/bash

ssh-keygen -t rsa -m PEM -f ansible/gitkey -N ""
ssh-keygen -t rsa -f ansible/agentkey -N ""
ssh-keygen -t rsa -f vagrantkey -N ""
